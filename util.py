import profile
import random
import string
import timeit


def check_input(data):
    data = data.replace(' ', '')
    if data.isdigit():
        return True
    else:
        return False


listes = [15, 21, 71, 43, 72, 87, 48, 69, 29, 9, 33, 82, 88, 88, 68, 59, 69, 43, 84, 30, 34, 15, 112, 501, 208, 147, 82,
          963, 719, 586, 980, 10, 529, 126, 357, 784, 565, 407, 228, 970, 345, 344, 163, 484, 894, 317, 410, 465, 827,
          595, 476, 164, 914, 313, 4, 395, 989, 337, 137, 695, 845, 879, 777, 127, 635, 966, 622, 257, 737, 687, 946,
          414, 738, 84, 865, 252, 484, 127, 62, 453, 528, 68, 420, 908, 917, 438, 237, 274, 350, 554, 333, 832, 303,
          584, 809, 36, 672, 829, 879, 570, 773, 239, 587, 687, 768, 458, 853, 51, 765, 251, 237, 666, 440, 549, 714,
          951, 16, 445, 345, 179, 911, 498, 525, 893, 585, 847, 811, 332, 811, 886, 885, 489, 991, 408, 302, 367, 11,
          660, 482, 519, 650, 145, 202, 559, 242, 633, 741, 687, 482, 639, 154, 700, 482, 247, 157, 297, 697, 908, 728,
          150, 355, 40, 362, 47, 739, 501, 540, 154, 739, 140, 129, 35, 886, 804, 62, 891, 388, 320, 844, 192, 200, 43,
          106, 846, 816, 765, 254, 839, 335, 998, 829, 861, 172, 578, 865, 461, 86, 927, 219, 938, 64, 526, 952, 217,
          18, 797, 752, 665, 924, 789, 771, 877, 958, 128, 653, 170, 96, 200, 281, 820, 796, 313, 54, 446, 951, 450,
          359, 937, 306, 678, 133, 80, 550, 224, 537, 213, 694, 154, 585, 131, 667, 696, 962, 445, 604, 877, 181, 296,
          516, 222, 982, 487, 81, 637, 488, 457, 776, 570, 42, 391, 66, 568, 1000, 942, 785, 1000, 614, 638, 322, 151,
          99, 456, 421, 126, 397, 105, 230, 22, 789, 743, 910, 228, 927, 323, 828, 678, 957, 941, 523, 398, 480, 213,
          684, 440, 702, 100, 812, 85, 142, 333, 798, 629, 10, 319, 826, 929, 28, 97, 756, 689, 656, 153, 105, 803, 691,
          909, 342, 82, 900, 665, 738, 171, 788, 933, 719, 689, 696, 67, 672, 797, 387, 474, 651, 662, 919, 971, 944,
          589, 122, 444, 877, 719]
# for i in range(1, 321):
#     listes.append(random.randint(1, 1000))
# print(listes)

lst = 'WGROBIRIONNRRIVKKBFFTIHHRZYOLJPKZNPDHQGMMXSBTDZSOLRDXAIONFPMEWEDQSENEGUQIETINYUZOBIQQYMOZGLTYIHCQIHNGSQVZQHDAIMBGATETOCCIHLUITVGMXJCFHVEAMONFHQGZTBCLSIGGONKWMPQOIOBDWJVBJZTCRTLDCCBLMZRPAZSZDJPNPTCIJOMDCEMOFKPJHGKQCDONIBQGYPIXDZLWEXWYAYIPWAXRNRQWCJRMYHMYSPCDBOPDHSWALVINEEUOKUSCQKGDEPKPNDUBWMIEZSJGERALYSRWNFHZUNYLALJTFCHK'
# print(len(stringi))
indexs = 224
index = 'FCHK'


def swap():
    return None


# def LinearSearch(lys, element):  # Линейный поиск
#     for i in range(len(lys)):
#         if lys[i] == element:
#             return i
#     return -1

def linearsearch(lst, x):
    result = None
    for index, string in enumerate(lst):
        swap()
        if string == x:
            swap()
            result = index
    return result


# print(linearsearch(listes, indexs))


# start = timeit.default_timer()

def binary_search(lst, x):
    sorted(lst)
    p = 0
    r = len(lst) - 1
    answer = None
    while p <= r:
        q = (p + r) // 2
        swap()
        if lst[q] == x:
            answer = q
            swap()
            break
        elif lst[q] > x:
            r = q - 1
            swap()
        elif lst[q] < x:
            p = q + 1
    return answer
# print(sorted(listes).count(244))
# print(binary_search(sorted(listes), indexs))


# stop = timeit.default_timer()
# print(stop-start)
# print(binsearchvirt(listes, 15))
# print(sorted(listes))
# print(binsearchvirt(sorted(listes), 15))

def search(my_list, item):
    is_found = False
    steps = 0
    for i in my_list:
        if item == i:
            is_found = True
        steps += 1
    print(f'Кол-во итераций = {steps}')
    return is_found


# print(search(listes,indexs))

def naive(txt, pat):
    M = len(pat)
    N = len(txt)

    for i in range(N - M + 1):
        status = 1
        for j in range(M):
            if txt[i + j] != pat[j]:
                status = 0
                swap()
                break
        if j == M - 1 and status != 0:
            swap()
            return i


# print(naive(lst,index))
# def func_prefix(s: str) -> list:
#     """
#     Fills Pi-function array
#     :param str: lookip string
#     :return: result
#     """
#     l = len(s)
#     P = [0] * l
#     i, j = 0, 1
#     while j < l:
#         if s[i] == s[j]:
#             P[j] = i + 1
#             i += 1
#             j += 1
#         # s[i] != s[j]:
#         elif i:  # i > 0
#             i = P[i - 1]
#         else:  # i == 0
#             P[j] = 0
#             j += 1
#     return P
#


# import random
# import string
#
#


# generate_random_string(3201)
# print(generate_random_string(321))
# def computeLPSArray(pat, M, lps):
#     len = 0  # длина предыдущего длинного суффикса префикса
#     lps[0]  # lps [0] всегда 0
#     i = 1
#     # цикл вычисляет lps [i] для i = 1 до M-1
#     while i < M:
#         if pat[i] == pat[len]:
#             len += 1
#             lps[i] = len
#             i += 1
#         else:
#             # Это сложно. Рассмотрим пример.
#             # AAACAAAA и я = 7. Идея похожа
#             # к шагу поиска.
#             if len != 0:
#                 len = lps[len - 1]
#                 # Также обратите внимание, что мы не увеличиваем i здесь
#             else:
#                 lps[i] = 0
#                 i += 1
#
# def KMPSearch(txt, pat):
#     M = len(pat)
#     N = len(txt)
#     # create lps [], который будет содержать самый длинный суффикс префикса
#     # значения для шаблона
#     lps = [0] * M
#     j = 0  # index для pat []
#     # Предварительная обработка шаблона (вычисление массива lps [])
#     computeLPSArray(pat, M, lps)
#     i = 0  # index для txt []
#     while i < N:
#         if pat[j] == txt[i]:
#             i += 1
#             j += 1
#         if j == M:
#             print("Found pattern at index " + str(i - j))
#             return i
#             j = lps[j - 1]
#         # несоответствие после j матчей
#         elif i < N and pat[j] != txt[i]:
#             # Не совпадать с символами lps [0..lps [j-1]],
#             # они будут совпадать в любом случае
#             if j != 0:
#                 j = lps[j - 1]
#             else:
#                 i += 1
#     return i
# solution = 'abcabeabcabcabdrabcabdtuyeabcabdwrabababalidsahglah;babababt'
# elem = 'ea'
def predkompil2(x):
    d = {0: 0}
    for i in range(1, len(x)):
        swap()
        j = d[i - 1]  # эту строку мы заменили
        while j > 0 and x[j] != x[i]:
            swap()
            j = d[j - 1]
        if x[j] == x[i]:
            swap()
            j += 1
        d[i] = j
    return d


def kmp(s, x):
    d = predkompil2(x)
    i = j = 0
    while i < len(s) and j < len(x):
        swap()
        if x[j] == s[i]:
            swap()
            i += 1
            j += 1
        elif j == 0:
            swap()
            i += 1
        else:
            swap()
            j = d[j - 1]
    else:
        swap()
        if j == len(x):
            swap()
            return i - j
        return None

# print(kmp(stringi, aboba))
solution = [0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ,12 ,13]
sorted(solution)
print(solution.index(11))